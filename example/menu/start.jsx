import { Stear, SWindow, _,s } from "../../stear/main.js";
import { fadein, fadeout, wait } from "../../stear/utils.js";
//import connect from "./connect.js";
import menu from "./menu";

export const preRender = true;

var wobel = Stear.addAnimation({
    "0%": {
        transform: "translate(-50%, -50%) scale(100%)"
    },
    "50%": {
        transform: "translate(-50%, -50%) scale(110%)"
    },
    "100%": {
        transform: "translate(-50%, -50%) scale(100%)"
    }
});

export const call = (stear, { find, resolve, render, call, event }, args) => {
    event.onloaded = async ()=>{
        await wait(Math.max(1, 1000 - performance.now()));
        await fadeout(find.main);
        call(menu/*, { id: "123-456", name: "Device" }*/);
        resolve();
    }     

    return <div style={{zIndex:1000}} find="main">
        <img src="logo.png" style={{
            width: "75vw",
            maxWidth: "75vh",
            maxHeight: "75vh",
            position: "absolute",
            left: "50%",
            top: "50%",
            transform: "translate(-50%, -50%)",
            animation: `${wobel} 1s ease-in-out 0s infinite`,
        }}/>
    </div>;
}

export default new SWindow({ preRender: true, call, backgroundColor:"#dde"});