import { Stear, SWindow } from "../../stear/main.js";

import start from "./start";


var stear = new Stear(document.querySelector("#stear"));

stear.style({
    backgroundColor: "#dde",
    fontFamily: "Helvetica Neue",
    fontWeight: "lighter",
    fontSize: "0.9em",
    color: "#2a2a2a",
});

/*stear.addElement("connect", connect);
stear.addElement("start", start);*/

stear.call(start, {});
