import l1 from "../../stear/extra/Elements/loading1.js";
import showStatus from "../../stear/extra/Pages/showStatus.js";
import { Stear, SWindow, _ } from "../../stear/main.js";
import { fadein, fadeout, wait } from "../../stear/utils.js";

const pool = Stear.addLanguagePool("connect");
const connectingto = pool.add("connectingto", "Connecting to: {} ({})");
const connected = pool.add("connected", "connected");
const connecterror = pool.add("connecterror", "Error while Connecting to: {}");

export const call = async (stear, { find, resolve, render, call, event }, {name,id}) => {

    event.onloaded = async ()=>{
        await fadein(find.main, 200, true);
        //do connection
        await wait(1000);
        if (Math.random() > 0.25) {

            call(showStatus, { text: connected });

            return resolve(true);
        } else {

            call(showStatus, { text: connecterror.r(name), color: "red" });

            return resolve(false);
        }
    };
    event.onresolve = async ()=>{
        await fadeout(find.main);
    }
    
    return _({ find: "main" }, [
        _({
            type: "p", 
            style: {
                position: "absolute",
                minWidth: "90%",
                textAlign: "center",
                top: "25%",
                left: "50%",
                transform: "translate(-50%,-50%)",
            }
        }, connectingto.r(name, id)),
        _({
            style: {
                position: "absolute",
                left: "50%",
                top: "50%",
                transform: "translate(-50%, -50%)",
            }
        }, l1())
    ]);
}

export default new SWindow({ preRender: true, call, backgroundColor: "#dde" });