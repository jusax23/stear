import prompt from "../../stear/extra/Pages/prompt.js";
import { Stear, SWindow, _, s } from "../../stear/main.js";

const call = async (stear, { find, resolve, close, render, call, event }, args) => {

    /*event.onloaded = () => { }*/
    /*event.onresolve = async () => { }*/
    /*event.onclose = async () => { }*/
    /*event.onBeforRerender = async () => { }*/
    /*event.onAfterRerender = async () => { }*/
    /*event.onParentRender = () => { }*/

    event.onParentRender = () => call(prompt, { text: "settings" }).then(console.log);
    

    return <div style={{
        display:"flex",
        alignItems: "center",
        justifyContent: "center",
        height: "100%",
    }}>
    Settings
    </div>
}

export default new SWindow({ call, backgroundColor: "#dde" });