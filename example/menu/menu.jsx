import { Stear, SWindow, _, s } from "../../stear/main.js";
import apps from "./apps";
import settings from "./settings";
import splash from "./splash";

const call = async (stear, { find, resolve, close, render, call, include, event }, args) => {

    /*event.onloaded = () => { }*/
    /*event.onresolve = async () => { }*/
    /*event.onclose = async () => { }*/
    /*event.onBeforRerender = async () => { }*/
    /*event.onAfterRerender = async () => { }*/

    let menu = [
        include(splash),
        include(apps),
        include(settings),
    ];
    window.dddd = menu[0];
    let show = 0;


    return <div style={{
        display: "flex",
        height: "100%",
        width: "100%",
        flexDirection: "column",
    }}>
        <div style={{
            position: "relative",
            width: "100%",
            height: "100%",
        }}>{() => {
            menu[show].opts.render();
            return menu[show].render({
                style: {
                    height: "100%",
                    width: "100%"
                }
            });
        }}</div>
        <div style={{
            height: "2rem",
            width: "calc(100% - 2rem)",
            padding: "1rem",
            backgroundColor: "#fee",
            display: "flex",
            flexDirection: "row",
            alignItems: "center",
            justifyContent: "space-evenly",
            borderRadius: "2rem 2rem 0 0",
        }}>
            {() => menu.map((m, i) => {
                return <div event={{
                    click: () => {
                        show = i;
                        render();
                    }
                }} style={{
                    height: "1rem",
                    width: "1rem",
                    backgroundColor: show == i ? "red" : "green"
                }}></div>;
            })}
        </div>
    </div>;
}

export default new SWindow({ call, backgroundColor: "#dde"});