# Stear

## Introduction
A simple Framework. Combine all Content in one File in an Modular way and native JavaScript.

## Getting started
Download the stear Submodule and add it to your Project.
## Features
### Create Stear Instance
```javascript
import { Stear, SWindow } from "./stear/main.js";
var stear = new Stear(document.querySelector("#stear"));
```
### Addind Windows

```javascript
stear.addElement("connect", connect);
stear.addElement("start", start);
```
Windows don't necessarily have to be add this Way.

An SWindow is an Extendion of the SFrame class. SWindow adds an DIV Element around the page content in Fullscreen.

### Calling Windows
```javascript
import start from "./start.js";

stear.call(stear.g("start"), {});
stear.call(start, {});
```
The first Argument must be an SFrame Instance. The secound Argument can be used to pass Data to the new Window. 

This Methode returns a Promise. When the Promise will be resolved, the return Value can be accessed. The Promise can be awaited. 

### Creating a Window
```javascript
import { Stear, SWindow, _ } from "../stear/main.js";

const call = async (stear, { find, resolve, render, call, event }, args) => {

    return _({

    }, [

    ]);
}

export default new SWindow({ call, preRender:true, backgroundColor: "#dde" });
```
**preRender**: This is set to true by default. When set to true the content will be rendered imidiatly. Atherwise only when called render().

**call**: When calling a Window this function will be called. The first Argument is the used Stear Instance. The secound Argument contains some StearingTools. The third Argument are the Arguments passt in the call request. This function must return the page Content or function which returns the page content.

### Creating Page Content / class_ class
```javascript
_({
    find:"someString",
    type:"div",
    style:{
        color:"#fff",
        backgroundColor:"black",
    },
    event: {
        click: ()=>{

        }
    }
    //other Attributes
},[
    //pass here other class_ Instances or Strings or Language Sentences
    //or functions that generats things like this
])
```
The function _(settings, childs) returns an class\_ Instance.

Attributes of the Settings Object while be applyed as HTML-Element Attribute.
Exeptions are find, type, style, event (all can be empty):

**type**: The type Attribute determines the DOMElement Type. The Default is div.

**find**: The find Attribute is an identifier. With find.someString you can get this Element. With find.someString._ you get the underlying DOMElement.

**style**: The content of the style Attribute will be applied as css style.

**event**: The content of the style Attribute will be applied with addEventListener.

Other Methodes:
```javascript
var element = _({},[]);
var newRenderedHTMLElement = await element.build(someArgsInArray); //Executs functions to create dynamic Structures
var newRenderedHTMLElement = element.render ;
var HTMLElement = element._ ; //Same as line befor when Element was completly rendered
element.childs = [
    //Overrides the secound Argument 
]; 
element.settings = {
    //Overrides first Argument (when possible)
}
```

### Intelligent Content / StearingTools

- find: Contains all Elements labeled with find. 
- resolve: This function resolves the call, like a regular Promise. The secound Argument determies wheser the SWindow shut be closed.
- close: This will close the SWindow.
- render: This function, will (re-)render the page content (executes every function in Content Tree (again).)
- call: The way to call an other SWindow/SFrame with an appropriate zIndex.
- event: An objects with callback functions, that can be overriden:
    - onloaded: Called after initial Content render.
    - onresolve: Will be awaited to execute operations on resolve or to delay it, when async.
    - onclose: Will be executed on calling close(). It will delay the closing process if async.
    - onBeforRerender: This will be executed befor the (re-)render while called render(). When async the render will be delayed.
    - onAfterRerender: This will be executed befor the (re-)render while called render(). When async it will delay the resolve of the render() function.

### Globel functions

#### Adding Global CSS the Oldway
```javascript
Stear.addGlobalStyleText(`
.cssClass {
    color: #fff;
    background-color:black;
}
`);
```
#### Adding Global CSS the with JSON
```javascript
var name = ".cssClass";
name = Stear.addGlobalStyleJSON({
    color: "#fff",
    backgroundColor: "black",
},name);
```
The Function can add CSS like CSS can be added to Elements in Pages. When leaving name empty a cssClass will be added with a sortof random name.

#### Adding CSS Animations with JSON
```javascript
var KeyframeName = Stear.addAnimation({
    "0%":{
        transform: "scale(100%)",
    },
    "50%": {
        transform: "scale(110%)",
    },
    "100%": {
        transform: "scale(100%)",
    }
}/*,name*/);
```

#### Language 
This Tool can be used in MultiLingual Projects.

Default Schema:
```javascript
const pool = Stear.addLanguagePool("poolname");
const sentences1 = pool.add("sentences1", "Some Text"); //id, default Text
const sentences2 = pool.add("sentences2", "Some {} Text with {}.");

sentences1 // Some Text
sentences2 // Some {} Text with {}.
sentences2.r("nice","a good flavour") // Some nice Text with a good flavour.
```
The Language System is a two level System. It is recomendet, to use the Name of the Window as poolname. 
The LanguageString can be used like a normal String. The Methode .r() can repalace "{}" in the Text.

When useing Stear.addLanguagePool or pool.add twise the same instance will be returned.

Other Language functions
```javascript
var someJSON = Stear.getLanguageFile;//contains all Defaults created yet.
Stear.lang = "de"; //change Language
Stear.addLanguageFile(fileJSON/*like someJSON*/,lang); //Language code like.
```

### Utils
#### wait
```javascript
await wait(200);
```
Simpel Methode of Timedelay in an async fcuntion
#### fadein
```javascript
await fadein(elems,ms,force);
await fadeout(elems,ms,force);
```
- **elems**: contains Element to Fade in or an Array of Elements
- **ms** Fade time
- **force** If set to true a alredy invisible/visible Element will be refaded out/in.
### Extras

loading1.js: simple to use Loadinganimation
